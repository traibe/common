module.exports = {
    root                        : 'api',
    
    //health                      : 'api/health',

    //user                        : 'api/user',
    //userLogin                   : 'api/user/login',
    //userLogout                  : 'api/user/logout',
    //userRegister                : 'api/user/register',
    //userRegisterConfirm         : 'api/user/register/confirm',
    //userRegisterConfirmRequest  : 'api/user/register/confirm/request',
    //userProfile                 : 'api/user/profile',
    //userProfileEmail            : 'api/user/profile/email',
    //userProfileNotifications    : 'api/user/profile/notifications',
    userPassword                : 'api/user/password',
    //userPasswordReset           : 'api/user/password/reset',
    //userPasswordChange          : 'api/user/password/change',
    //userPasswordChangeToken     : 'api/user/password/change/token',
    //userSuspendOrActivate       : 'api/user/suspendOrActivate',

    verify                      : 'api/verify',
    //verifyJwt                   : 'api/verify/jwt',
    //verifyToken                 : 'api/verify/token',
    //verifyEmailTaken            : 'api/verify/email/taken',
    //verifyHandleTaken           : 'api/verify/handle/taken',

    //product                     : 'api/product',
    //productSimilar              : 'api/product/similar',
    //productOptions              : 'api/product/options',
    //productListings             : 'api/product/listings',

    //contact                     : 'api/contact',

    //settings                    : 'api/settings',

    app                         : 'api/app',
    appStatistics               : 'api/app/statistics',
    //appStatisticsUserCount      : 'api/app/statistics/user/count'
}