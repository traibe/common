module.exports = {
    root                        : '/',
    userLogin                   : '/user/login',
    userPassword                : '/user/password',
    userPasswordResetRequest    : '/user/password/reset/request',
    userPasswordResetToken      : '/user/password/reset/:token',
    userProfile                 : '/user/profile',
    userProfileNotifications    : '/user/profile/notifications',
    userRegister                : '/user/register',
    userRegisterConfirmToken    : '/user/register/confirm/:token',
    legalPrivacy                : '/legal/privacy',
    product                     : '/product/:product_id',
    contact                     : '/contact',
    error403                    : '/error/403',
    error404                    : '/error/404',
    error500                    : '/error/500',
    maintenance                 : '/maintenance'
}